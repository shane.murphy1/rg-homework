import {dirname, join} from "https://deno.land/std@v0.159.0/path/mod.ts";

import Prompt from "https://deno.land/x/prompt@v1.0.0/mod.ts"
import {TerminalSpinner} from "https://deno.land/x/spinners@v1.1.2/mod.ts";

// Once binaries are installed, create objects that expose their CLI options as methods.
// Make sure we don't copy binaries we didn't install

const BIN_DIR = dirname(Deno.execPath())
const SCRIPT_DIR = dirname(BIN_DIR)
const CARGO_ROOT = join(BIN_DIR, "cargo")
const CARGO_BIN = join(CARGO_ROOT, "bin")
const FNM = join(CARGO_BIN, "fnm")
const SEMVER_REGEX
    = /(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)/.source
    + /(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?/.source
    + /(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?/.source;


const run = async (...args: string[]): Promise<boolean> => {
    const bin = args.shift()
    if (bin === undefined) throw new Error("Must provide at least a program name")
    const cmd = new Deno.Command(bin, {args: [...args], stdout: "null", stderr: "null"})
    const status = await cmd.spawn().status
    return status.success
}

const runOutput = async (...args: string[]): Promise<[boolean, string]> => {
    const bin = args.shift()
    if (bin === undefined) throw new Error("Must provide at least a program name")
    const cmd = new Deno.Command(bin, {args: [...args]})
    const {code, stdout} = await cmd.output()
    return [code === 0, new TextDecoder().decode(stdout)]
}

const installed = async (program: string): Promise<boolean> => {
    return await run("which", program)
}

const install_node = async (): Promise<boolean> => {
    const fnm_install = await run("cargo", "install", "fnm", "--root", CARGO_ROOT)
    const node_install = await run(FNM, "install", "--lts", "--fnm-dir", BIN_DIR)
    return fnm_install && node_install
}

const install_wasm_pack = async (): Promise<boolean> => {
    return await run("cargo", "install", "wasm-pack", "--root", CARGO_ROOT)
}

const ask_to_install = async (program: string): Promise<boolean> => {
    const answers = await Prompt.prompts([
        {
            message: `${program} is not installed, install it into this project?`,
            type: "confirm",
            name: "agree",
            defaultValue: true
        },
    ]);

    return answers.agree
}

const setup_node = async () => {
    if (!await installed("node")) {
        if (await ask_to_install("NodeJS")) {
            const install = new TerminalSpinner("Installing Node");
            install.start()
            if (!await install_node()) {
                install.fail("Failed to install Node!")
                throw new Error("Failed to install Node!")
            }
            install.succeed("Done!")
        }
    }
}

const setup_wasm_pack = async () => {
    if (!await installed("wasm-pack")) {
        if (await ask_to_install("wasm_pack")) {
            const install = new TerminalSpinner("Installing wasm-pack");
            install.start()
            if (!await install_wasm_pack()) {
                install.fail("Failed to install wasm-pack!")
                throw new Error("Failed to install wasm-pack!")
            }
            install.succeed("Done!")
        }
    }
}

const find_lts_version = (fnm_output: string): string | null => {
    const re = new RegExp(
        SEMVER_REGEX,
        "m"
    )
    for (const line of fnm_output.split("\n")) {
        if (line.includes("lts-latest")) {
            const matches = re.exec(line)
            if (matches) return matches[0]
        }
    }
    return null
}

const copy_binaries = async () => {
    const [status, output] = await runOutput(FNM, "ls", "--fnm-dir", BIN_DIR)
    if (!status) throw new Error("Failed to run fnm")

    const lts_version = find_lts_version(output)
    if (lts_version === null) throw new Error("Failed to resolve Node version in fnm install")

    const node_dir = join(BIN_DIR, "node-versions", `v${lts_version}`, "installation", "bin")
    const node = join(node_dir, "node")
    const npm = join(node_dir, "npm")
    const wasm_pack = join(CARGO_BIN, "wasm-pack")

    await Deno.symlink(node, join(SCRIPT_DIR, "node"))
    await Deno.symlink(npm, join(SCRIPT_DIR, "npm"))
    await Deno.symlink(wasm_pack, join(SCRIPT_DIR, "wasm-pack"))
}

async function main() {
    await setup_node()
    await setup_wasm_pack()
    await copy_binaries()
}

await main()