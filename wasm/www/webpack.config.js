const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
    entry: "./bootstrap.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bootstrap.js",
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    mode: "development",
    plugins: [
        new CopyWebpackPlugin({patterns: ['index.html']})
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "ts-loader",
                exclude: /node_modules/,
            }
        ]
    },
    experiments: {
        "asyncWebAssembly": true
    }
};
